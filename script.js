// console.log("Hello World");

function Pokemon(name, level) {
    //properties
    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = level;
    //methods
    this.tackle = function(target) {
        target.health -= this.attack;
        console.log(this.name + ' tackled '+ target.name);
        console.log(target.name + "'s health is now reduced to " + (target.health));

        if(target.health <= 5) {
            target.faint()
        }
    },
    this.faint = function() {
        console.log(this.name + " fainted.")
    }
}

let dialga = new Pokemon("Dialga", 16);
let palkia = new Pokemon("Palkia", 8);

dialga.tackle(palkia);
dialga.tackle(palkia);